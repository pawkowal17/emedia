from math import gcd
from random import randint

LP = [11, 13, 17, 19, 23, 29, 31, 37, 41, 43]


class RSA:
    def __init__(self):
        self.e = 0
        self.d = 0
        self.n = 0

    def code_rsa(self, a):

        self.e = 3
        self.n = 391
        self.d = 235

        m = a
        c = 1
        e = self.e
        n = self.n

        while e > 1:
            if e % 2:
                c = (c * m) % n
            m = (m * m) % n
            e /= 2

        return c

    def decode_rsa(self, c):
        m = 1
        d = self.d

        while d > 1:
            if d % 2:
                m = (m * c) % self.n
            c = (c * c) % self.n
            d /= 2

        return m

    def generate_key(self):
        while True:
            p = LP[randint(-1, 9)]
            q = LP[randint(-1, 9)]
            if p != q:
                break

        n = p * q
        phi = (p - 1) * (q - 1)

        e = 3
        while gcd(e, phi) != 1:
            self.d = self.reserved_modulo(e, phi)
            e += 2

        self.e = e
        self.n = n

    def reserved_modulo(self, a, n):
        p0 = 0
        p1 = 1
        q = n / a
        r = n % a

        while r > 0:
            t = p0 - q * p1

            if t >= 0:
                t = t % n
            else:
                t = n - ((-t) % n)

            p0 = p1
            p1 = t
            n = a
            a = r
            q = n / a
            r = n % a

        return p1
