# import matplotlib.pyplot as plt
from PIL import Image
from Header import Header
from Fourier import Fourier
from RSA import RSA


if __name__ == "__main__":

    file = Header("tulips.bmp")
    data = file.open_file()
    gray_image = Image.fromarray(data)
    fourier = Fourier(data)
    fourier_image = fourier.fft()

    print("Rozmiar pliku: " + str(file.size) + " B")
    print("Rozmiar samego obrazka: " + str(file.image_size) + " B")
    print("Rozmiar nagłówka: " + str(file.header_size) + " B")
    print("Początek obrazka: " + str(file.offset) + " bajt")
    print("Warstwy kolorów: " + str(file.colors_layers))
    print("Ilość bitów na kolor: " + str(file.bit_color))
    print("Szerokość obrazka: " + str(file.width))
    print("Wysokość obrazka: " + str(file.height))
    print("Rozdzielczość po x: " + str(file.x_resolution))
    print("Rozdzielczość po y: " + str(file.y_resolution))

#     fig = plt.figure()
#     ax1 = fig.add_subplot(121)
#     ax2 = fig.add_subplot(122)
#     ax1.imshow(gray_image)
#     ax2.imshow(fourier_image)
#     ax1.title.set_text('Gray Image')
#     ax2.title.set_text('Fourier Image')
#     plt.show()

    # szyfrowanie rsa
    rsa = RSA()
#     rsa.generate_key()
#     for element in data:
#         new = rsa.code_rsa(element)
    example = 123
    coded = rsa.code_rsa(example)
    decoded = rsa.decode_rsa(coded)
    print(coded)
    print(decoded)
