import numpy as np
from PIL import Image


class Fourier:
    def __init__(self, data):
        self.data = data
        self.norm_fourier_img = Image

    def fft(self):
        fft = np.fft.fft2(self.data)
        fft = np.fft.fftshift(fft)
        fft = abs(fft)
        fft = np.log10(fft)
        lowest = np.nanmin(fft[np.isfinite(fft)])
        highest = np.nanmax(fft[np.isfinite(fft)])
        original_range = highest - lowest
        norm_fourier = (fft - lowest) / original_range * 255
        self.norm_fourier_img = Image.fromarray(norm_fourier)
        return self.norm_fourier_img
