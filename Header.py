import numpy as np


class Header:
    def __init__(self, filename):
        self.filename = filename
        self.pixels_array = np.array([])
        self.size = 0
        self.offset = 0
        self.header_size = 0
        self.width = 0
        self.height = 0
        self.colors_layers = 0
        self.bit_color = 0
        self.image_size = 0
        self.x_resolution = 0
        self.y_resolution = 0

    def open_file(self):
        file = open(self.filename, "rb")
        self.read_file(file)
        file.close()
        return self.pixels_array

    def read_file(self, file):
        if self.read_bytes(file, 2) != 19778:
            print("Not BMP file")

        self.size = self.read_bytes(file, 4)
        self.read_bytes(file, 4)
        self.offset = self.read_bytes(file, 4)
        self.header_size = self.read_bytes(file, 4) + 14
        self.width = self.read_bytes(file, 4)
        self.height = self.read_bytes(file, 4)
        self.colors_layers = self.read_bytes(file, 2)
        self.bit_color = self.read_bytes(file, 2)
        self.read_bytes(file, 4)
        self.image_size = self.read_bytes(file, 4)
        self.x_resolution = self.read_bytes(file, 4)
        self.y_resolution = self.read_bytes(file, 4)
        self.read_bytes(file, 8)

        move = self.offset - 54
        if move != 0:
            self.read_bytes(file, move)

        return self.gray_array(file)

    def read_bytes(self, file, n):
        byte_array = bytearray(file.read(n))
        converted = 0
        x = len(byte_array) - 1
        for i in range(len(byte_array)):
            converted = (converted << 8) + int(byte_array[x - i])
        return converted

    def gray_array(self, file):
        self.pixels_array = np.zeros(shape=(self.height, self.width))
        for i in range(self.height):
            for j in range(self.width):
                b = self.read_bytes(file, 1)
                g = self.read_bytes(file, 1)
                r = self.read_bytes(file, 1)
                gray = 0.21*r + 0.72*g + 0.07*b
                self.pixels_array[self.height-1-i, j] = gray
